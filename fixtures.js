const mongoose = require('mongoose');
const config = require('./config');

const User = require('./models/User');
const Category = require('./models/Category');

mongoose.connect(config.db.url +  '/' + config.db.name);

const db = mongoose.connection;

const collections = ['users', 'category'];

db.once('open', async () => {
  collections.forEach( async collectionName => {
    try {
      await db.dropCollection(collectionName);
    } catch (e) {
      console.log(`Collection ${collectionName} did not exist in DB`);
    }
  });


  const [adminUser, user1User, user2User] = await User.create({
    username: 'admin',
    password: '12345',
    display_name: 'Administrator',
    phone_number: '+996 775 777 777'
  }, {
    username: 'john_doe',
    password: '123',
    display_name: 'John Doe',
    phone_number: '+996 775 333 333'
  }, {
    username: 'max_payne',
    password: '123',
    display_name: 'Max Payne',
    phone_number: '+996 775 555 555'
  });

  const [computers, furniture, food] = await Category.create({
    title: 'Computers',
    description: 'computer equipment'
  }, {
    title: 'Furniture',
    description: 'Furniture'
  }, {
    title: 'Food',
    description: 'Food'
  });

  db.close();
});